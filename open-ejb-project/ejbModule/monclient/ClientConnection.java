package monclient;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert;
import org.junit.Test;

import monpkg.services.ConnectedUser;

public class ClientConnection {

    @Test
    public void testLocalToUpperSleppy() throws InterruptedException {
        Thread t = new Thread(codeThreadFils);
        Thread t2 = new Thread(codeThreadFils);
        t.start();
        t2.start();
        t.join();
        t2.join();
        // WARNING TEST WILL ALWAYS SUCCEED
        // ERRORS ARE NOT PROPAGATED FROM THREAD
    }

    private Runnable codeThreadFils = new Runnable() {

        public void run() {
            ConnectedUser ejb = getEjb();
            ejb.login("Alice", "passphrase");
            ejb.logout();
//            ejb.login("AliceREBORN", "passphrase"); // → javax.ejb.NoSuchEJBException: Not Found
        }

        private ConnectedUser getEjb() {
            Object ref = null;
            // prepare client context
            try {
                Context context = new InitialContext();
                // lookup EJB
                ref = context.lookup("connectedUserLocal");
            } catch (NamingException e) {
                throw new RuntimeException(e);
            }
            // test and use it
            Assert.assertTrue(ref instanceof ConnectedUser);
            ConnectedUser ejb = (ConnectedUser) ref;
            return ejb;
        }
    };

}
