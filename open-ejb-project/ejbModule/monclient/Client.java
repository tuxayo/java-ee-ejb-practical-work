package monclient;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert;
import org.junit.Test;

import monpkg.services.ToUpper;

public class Client {

    @Test
    public void testLocalToUpper() throws NamingException {
        // prepare client context
        Context context = new InitialContext();
        // lookup EJB
        Object ref = context.lookup("toUpperLocal");
        // test and use it
        Assert.assertTrue(ref instanceof ToUpper);
        ToUpper p = (ToUpper) ref;
        Assert.assertEquals("PREMIER", p.toUpper("premier"));
    }

    @Test
    public void testLocalToUpperSleppy() throws NamingException, InterruptedException {
        Thread t = new Thread(codeThreadFils);
        Thread t2 = new Thread(codeThreadFils);
        t.start();
        t2.start();
        t.join();
        t2.join();
    }

    Runnable codeThreadFils = new Runnable() {
        public void run() {
            Object ref = null;
            // prepare client context
            try {
                Context context = new InitialContext();
                // lookup EJB
                ref = context.lookup("toUpperLocal");
            } catch (NamingException e) {
                // ignore exception
                e.printStackTrace();
            }
            // test and use it
            Assert.assertTrue(ref instanceof ToUpper);
            ToUpper p = (ToUpper) ref;
            Assert.assertEquals("PREMIER", p.toUpperSleppy("premier"));
        }
    };
}