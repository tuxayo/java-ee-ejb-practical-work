package monpkg.impl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Local;
import javax.ejb.Stateless;

import monpkg.services.ToUpper;

@Stateless(name = "toUpper", description = "Un premier essai")
@Local
public class ToUpperImpl implements ToUpper {

    @PostConstruct()
    public void debut() {
        System.out.println("Starting " + this);
    }

    @PreDestroy
    public void fin() {
        System.out.println("Stopping " + this);
    }

    public String toUpper(String qui) {
        return qui.toUpperCase();
    }

    @Override
    public String toUpperSleppy(String qui) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // ignore exception
            e.printStackTrace();
        }
        return qui.toUpperCase();
    }

}
