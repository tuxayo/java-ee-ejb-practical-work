package monpkg.impl;

import javax.annotation.PreDestroy;
import javax.ejb.Local;
import javax.ejb.Remove;
import javax.ejb.Stateful;

import monpkg.services.ConnectedUser;

@Stateful(name = "connectedUser")
@Local
public class ConnectedUserImpl implements ConnectedUser {

    private boolean loggedIn;
    private String loginName;

    @Override
    public void login(String login, String pwd) {
        this.loginName = login;
        loggedIn = true;
        System.out.println("Log in user " + loginName + " " + this);
    }

    @Remove
    public void logout() {
        if (!loggedIn) {
            throw new RuntimeException("Why did you tried to log out without being logged in?");
        }
        System.out.println("Logout user " + loginName + " " + this);
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Destroying " + loginName + " " + this);
    }

}