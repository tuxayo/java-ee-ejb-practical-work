package monclient;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert;
import org.junit.Test;

import monpkg.services.ToUpper;

public class Client {

    @Test
    public void testRemoteToUpper() throws NamingException {
        Context context = prepareClientContext();
        // lookup EJB
        Object ref = context.lookup("toUpperRemote");
        // test and use it
        Assert.assertTrue(ref instanceof ToUpper);
        ToUpper p = (ToUpper) ref;
        Assert.assertEquals("PREMIER", p.toUpper("premier"));
    }

    private Context prepareClientContext() throws NamingException {
//        The following commented block is not needed if jndi.properties is in
//        the CLASSPATH
//        Properties props = new Properties();
//        String client = "org.apache.openejb.client.RemoteInitialContextFactory";
//        props.put(Context.INITIAL_CONTEXT_FACTORY, client);
//        props.put(Context.PROVIDER_URL, "ejbd://localhost:4201");
//        Context context = new InitialContext(props);
        Context context = new InitialContext();
        return context;
    }

}
